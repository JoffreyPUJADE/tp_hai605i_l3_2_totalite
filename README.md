# TP HAI605I L3 2 Totalité

Ce dépôt contient la totalité des TP de la partie Images 2D de l'UE "HAI605I - Données Multimédia" de la Faculté des Sciences de l'Université de Montpellier.

# Sommaire

* [Utilisation](#utilisation)
* [TP 1](#tp-1)
* [TP 2](#tp-2)
* [TP 3](#tp-3)
* [TP 4](#tp-4)
* [TP 5](#tp-5)
* [Sources](#sources)
	* [Sources des images](#sources-des-images)
* [To-Do List](#to-do-list)

# Utilisation

Chacun des répertoires de TP contient un Makefile, permettant de compiler la totalité des exécutables demandés pour chacun des TP. Il suffit donc de taper la commande `make` dans l'un des répertoires de TP afin de compiler lesdits exécutables pour les utiliser.

# TP 1

# TP 2

# TP 3

# TP 4

# TP 5

# Sources

## Sources des images

La plupart des images utilisées au cours du développement de ces différents TPs ont été trouvées sur le site du lirmm, sur la page de Monsieur William PUECH, disponible sur ce lien :.

Voici les sources des autres images :
* Lock Kathrine, [publicdomainvectors.org](https://publicdomainvectors.org/fr/tag/svg), [sur cette page](https://publicdomainvectors.org/fr/gratuitement-des-vecteurs/Loch-Katrine/54131.html)

# To-Do List

* TP 1 :
	- Voir si bug Histo
	- Voir si bug Histo Couleurs
	- Voir si bug Profil
	- Voir si bug Profil Couleurs
* TP 2 :
	- Voir si bug Différence pour niveaux de gris (difference.cpp sans seuillage)
	- Voir si bug Différence pour couleurs (differenceCouleur.cpp avec seuillage)
	- Voir si bug Différence pour couleurs (differenceCouleur.cpp sans seuillage)
	- Voir si bug sur toutes les adaptations vers les couleurs
* TP 3 :
	- Voir si bug "inverseCouleur.cpp"
* Totalité des TPs :
	- profil : lluint -> uint