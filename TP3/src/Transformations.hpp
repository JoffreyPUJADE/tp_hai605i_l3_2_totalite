#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "image_ppm.h"
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

typedef unsigned int uint;
typedef long long unsigned int lluint;

// Fonctions définies pour convertir des types de variables vers d'autres.

template<typename T>
std::string type2String(T value)
{
	std::ostringstream oss;
	
	oss << value;
	
	return oss.str();
}

template<typename T>
T string2Type(std::string value)
{
	std::istringstream iss(value);
	
	T res;
	
	iss >> res;
	
	return res;
}

template<typename T, typename S>
S type2Other(T value)
{
	return string2Type<S>(type2String<T>(value));
}


// Fonctions définies pour le TP 1.

static inline void seuil1(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
			imgOut[i*nW+j] = imgIn[i*nW+j] < s ? 0 : 255;
	}
}

static inline void seuil2(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < s1)
				imgOut[i*nW+j] = 0;
			else
				imgOut[i*nW+j] = imgIn[i*nW+j] < s2 ? 128 : 255;
		}
	}
}

static inline void seuil3(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2, int s3)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < s1)
				imgOut[i*nW+j] = 0;
			else if(imgIn[i*nW+j] < s2)
				imgOut[i*nW+j] = 85;
			else
				imgOut[i*nW+j] = imgIn[i*nW+j] < s3 ? 170 : 255;
		}
	}
}

static inline void seuil1Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil1(rouge, rougeRes, nH, nW, s);
	seuil1(vert, vertRes, nH, nW, s);
	seuil1(bleu, bleuRes, nH, nW, s);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void seuil2Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil2(rouge, rougeRes, nH, nW, s1, s2);
	seuil2(vert, vertRes, nH, nW, s1, s2);
	seuil2(bleu, bleuRes, nH, nW, s1, s2);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void seuil3Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2, int s3)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil3(rouge, rougeRes, nH, nW, s1, s2, s3);
	seuil3(vert, vertRes, nH, nW, s1, s2, s3);
	seuil3(bleu, bleuRes, nH, nW, s1, s2, s3);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline std::vector<lluint> histo(OCTET* imgIn, int nH, int nW)
{
	lluint tabOcurrences[256] = {0};
	
	for(int i=0;i<(nH*nW);++i)
		++tabOcurrences[((lluint)imgIn[i])];
	
	std::vector<lluint> tabRes;
	
	for(int i=0;i<256;++i)
		tabRes.push_back(tabOcurrences[i]);
	
	return tabRes;
}

static inline std::vector<std::vector<lluint> > histoCouleur(OCTET* imgIn, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	std::vector<std::vector<lluint> > tabRes;
	
	tabRes.push_back(histo(rouge, nH, nW));
	tabRes.push_back(histo(vert, nH, nW));
	tabRes.push_back(histo(bleu, nH, nW));
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	return tabRes;
}

static inline std::vector<lluint> profil(OCTET* imgIn, int nH, int nW, char choixCL, int indice)
{
	if((choixCL != 'l') && (choixCL != 'c'))
		throw std::runtime_error("ERREUR : Profil : Le choix doit etre uniquement 'l' pour ligne ou 'c' pour colonne.");
	
	if((choixCL == 'l') && ((indice < 0) || (indice >= nH)))
		throw std::range_error("ERREUR : Profil : L'indice de la ligne choisi est en-dehors des bornes de l'image (qui est [0;hauteur de l'image[).");
	else if((choixCL == 'c') && ((indice < 0) || (indice >= nW)))
		throw std::range_error("ERREUR : Profil : L'indice de la colonne choisi est en-dehors des bornes de l'image (qui est [0;largeur de l'image[).");
	
	std::vector<lluint> tabRes;
	
	if(choixCL == 'l') // Si le type de profil choisi est la ligne, alors...
	{
		for(int j=0;j<nW;++j)
			tabRes.push_back(imgIn[indice*nW+j]);
	}
	else // Sinon, si le type de profil choisi est la colonne, alors...
	{
		for(int i=0;i<nH;++i)
			tabRes.push_back(imgIn[i*nW+indice]);
	}
	
	return tabRes;
}

static inline std::vector<std::vector<lluint> > profilCouleur(OCTET* imgIn, int nH, int nW, char choixCL, int indice)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	std::vector<std::vector<lluint> > tabRes;
	
	try
	{
		tabRes.push_back(profil(rouge, nH, nW, choixCL, indice));
		tabRes.push_back(profil(vert, nH, nW, choixCL, indice));
		tabRes.push_back(profil(bleu, nH, nW, choixCL, indice));
	}
	catch(std::runtime_error const& err)
	{
		delete rouge;
		delete vert;
		delete bleu;
		
		rouge = nullptr;
		vert = nullptr;
		bleu = nullptr;
		
		throw std::runtime_error(err.what());
	}
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	return tabRes;
}

// Fonctions définies pour le TP 2.

static inline void erosion(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<nH-1;++i)
	{
		for(int j=1;j<nW-1;++j)
		{
			bool test = ((imgIn[(i-1)*nW+j]) && (imgIn[(i+1)*nW+j]) && (imgIn[i*nW+(j-1)]) && (imgIn[i*nW+(j+1)]));
			
			imgOut[i*nW+j] = test ? 255 : 0;
		}
	}
}

static inline void erosionGris(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<nH-1;++i)
	{
		for(int j=1;j<nW-1;++j)
		{
			uint resultat = ((imgIn[(i-1)*nW+j]) & (imgIn[(i+1)*nW+j]) & (imgIn[i*nW+(j-1)]) & (imgIn[i*nW+(j+1)]));
			
			imgOut[i*nW+j] = resultat;
		}
	}
}

static inline void erosionCouleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	erosionGris(rouge, rougeRes, nH, nW);
	erosionGris(vert, vertRes, nH, nW);
	erosionGris(bleu, bleuRes, nH, nW);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void dilatation(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<nH-1;++i)
	{
		for(int j=1;j<nW-1;++j)
		{
			bool test = ((imgIn[(i-1)*nW+j]) && (imgIn[(i+1)*nW+j]) && (imgIn[i*nW+(j-1)]) && (imgIn[i*nW+(j+1)]));
			
			imgOut[i*nW+j] = test ? 0 : 255;
		}
	}
}

static inline void dilatationGris(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<nH-1;++i)
	{
		for(int j=1;j<nW-1;++j)
		{
			uint resultat = ((imgIn[(i-1)*nW+j]) & (imgIn[(i+1)*nW+j]) & (imgIn[i*nW+(j-1)]) & (imgIn[i*nW+(j+1)]));
			
			imgOut[i*nW+j] = resultat;
		}
	}
}

static inline void dilatationCouleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	dilatationGris(rouge, rougeRes, nH, nW);
	dilatationGris(vert, vertRes, nH, nW);
	dilatationGris(bleu, bleuRes, nH, nW);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void fermeture(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET* imgTemp = nullptr;
	int nTaille = nH * nW;
	
	allocation_tableau(imgTemp, OCTET, nTaille);
	
	dilatation(imgIn, imgTemp, nH, nW);
	erosion(imgTemp, imgOut, nH, nW);
	
	delete imgTemp;
	
	imgTemp = nullptr;
}

static inline void fermetureGris(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET* imgTemp = nullptr;
	int nTaille = nH * nW;
	
	allocation_tableau(imgTemp, OCTET, nTaille);
	
	dilatationGris(imgIn, imgTemp, nH, nW);
	erosionGris(imgTemp, imgOut, nH, nW);
	
	delete imgTemp;
	
	imgTemp = nullptr;
}

static inline void fermetureCouleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	fermetureGris(rouge, rougeRes, nH, nW);
	fermetureGris(vert, vertRes, nH, nW);
	fermetureGris(bleu, bleuRes, nH, nW);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void ouverture(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET* imgTemp = nullptr;
	int nTaille = nH * nW;
	
	allocation_tableau(imgTemp, OCTET, nTaille);
	
	erosion(imgIn, imgTemp, nH, nW);
	dilatation(imgTemp, imgOut, nH, nW);
	
	delete imgTemp;
	
	imgTemp = nullptr;
}

static inline void ouvertureGris(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET* imgTemp = nullptr;
	int nTaille = nH * nW;
	
	allocation_tableau(imgTemp, OCTET, nTaille);
	
	erosionGris(imgIn, imgTemp, nH, nW);
	dilatationGris(imgTemp, imgOut, nH, nW);
	
	delete imgTemp;
	
	imgTemp = nullptr;
}

static inline void ouvertureCouleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	ouvertureGris(rouge, rougeRes, nH, nW);
	ouvertureGris(vert, vertRes, nH, nW);
	ouvertureGris(bleu, bleuRes, nH, nW);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void difference(OCTET* imgInSeuillee, OCTET* imgInDilatee, OCTET* imgOut, int nH, int nW)
{
	for(int i=0;i<(nH*nW);++i)
		imgOut[i] = (imgInSeuillee[i] != imgInDilatee[i]) ? 255 : 0;
}

static inline void differenceCouleur(OCTET* imgInSeuillee, OCTET* imgInDilatee, OCTET* imgOut, int nH, int nW)
{
	OCTET *rougeSeuille(nullptr), *vertSeuille(nullptr), *bleuSeuille(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rougeSeuille, OCTET, nTaille);
	allocation_tableau(vertSeuille, OCTET, nTaille);
	allocation_tableau(bleuSeuille, OCTET, nTaille);
	
	planR(rougeSeuille, imgInSeuillee, nTaille);
	planV(vertSeuille, imgInSeuillee, nTaille);
	planB(bleuSeuille, imgInSeuillee, nTaille);
	
	OCTET *rougeDilate(nullptr), *vertDilate(nullptr), *bleuDilate(nullptr);
	
	allocation_tableau(rougeDilate, OCTET, nTaille);
	allocation_tableau(vertDilate, OCTET, nTaille);
	allocation_tableau(bleuDilate, OCTET, nTaille);
	
	planR(rougeDilate, imgInDilatee, nTaille);
	planV(vertDilate, imgInDilatee, nTaille);
	planB(bleuDilate, imgInDilatee, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	difference(rougeSeuille, rougeDilate, rougeRes, nH, nW);
	difference(vertSeuille, vertDilate, vertRes, nH, nW);
	difference(bleuSeuille, bleuDilate, bleuRes, nH, nW);
	
	delete rougeSeuille;
	delete vertSeuille;
	delete bleuSeuille;
	
	rougeSeuille = nullptr;
	vertSeuille = nullptr;
	bleuSeuille = nullptr;
	
	delete rougeDilate;
	delete vertDilate;
	delete bleuDilate;
	
	rougeDilate = nullptr;
	vertDilate = nullptr;
	bleuDilate = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

// Fonctions définies pour le TP 3.

static inline void inverse(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			imgOut[i*nW+j] = (255 - (imgIn[i*nW+j]) % 256);
		}
	}
}

static inline void flou1(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<(nH-1);++i)
	{
		for(int j=1;j<(nW-1);++j)
		{
			imgOut[i*nW+j] = ((imgIn[i*nW+j] + imgIn[(i-1)*nW+j] + imgIn[(i+1)*nW+j] + imgIn[i*nW+(j-1)] + imgIn[i*nW+(j+1)]) / 5);
		}
	}
}

static inline void flou2(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	for(int i=1;i<(nH-1);++i)
	{
		for(int j=1;j<(nW-1);++j)
		{
			imgOut[i*nW+j] = ((imgIn[i*nW+j] + imgIn[(i-1)*nW+j] + imgIn[(i+1)*nW+j] + imgIn[i*nW+(j-1)] + imgIn[i*nW+(j+1)] + imgIn[(i-1)*nW+(j-1)] + imgIn[(i-1)*nW+(j+1)] + imgIn[(i+1)*nW+(j-1)] + imgIn[(i+1)*nW+(j+1)]) / 9);
		}
	}
}

static inline void inverseCouleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge, *vert, *bleu;
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes, *vertRes, *bleuRes;
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	inverse(rouge, rougeRes, nH, nW);
	inverse(vert, vertRes, nH, nW);
	inverse(bleu, bleuRes, nH, nW);
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	free(rouge);
	free(vert);
	free(bleu);
	free(rougeRes);
	free(vertRes);
	free(bleuRes);
}

static inline void flou1Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge, *vert, *bleu;
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes, *vertRes, *bleuRes;
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	flou1(rouge, rougeRes, nH, nW);
	flou1(vert, vertRes, nH, nW);
	flou1(bleu, bleuRes, nH, nW);
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	free(rouge);
	free(vert);
	free(bleu);
	free(rougeRes);
	free(vertRes);
	free(bleuRes);
}

static inline void flou2Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW)
{
	OCTET *rouge, *vert, *bleu;
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes, *vertRes, *bleuRes;
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	flou2(rouge, rougeRes, nH, nW);
	flou2(vert, vertRes, nH, nW);
	flou2(bleu, bleuRes, nH, nW);
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	free(rouge);
	free(vert);
	free(bleu);
	free(rougeRes);
	free(vertRes);
	free(bleuRes);
}

#endif // TRANSFORMATIONS_HPP