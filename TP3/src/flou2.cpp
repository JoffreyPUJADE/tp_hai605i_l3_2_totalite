#include "Transformations.hpp"
#include <sstream>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageEntree nomImageSortie";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageEntree = argv[1];
	std::string nomImageSortie = argv[2];
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	OCTET *ImgIn, *ImgOut;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageEntree.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	flou2(ImgIn, ImgOut, nH, nW);
	
	ecrire_image_pgm(nomImageSortie.c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}