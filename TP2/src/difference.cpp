#include "Transformations.hpp"
#include <sstream>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 4)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageSeuilleeEntree nomImageDilateeEntree nomImageSortie";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageSeuilleeEntree = argv[1];
	std::string nomImageDilateeEntree = argv[2];
	std::string nomImageSortie = argv[3];
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageSeuilleeEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	OCTET *ImgInSeuillee, *ImgInDilatee, *ImgOut;
	
	allocation_tableau(ImgInSeuillee, OCTET, nTaille);
	lire_image_pgm(nomImageSeuilleeEntree.c_str(), ImgInSeuillee, nH * nW);
	allocation_tableau(ImgInDilatee, OCTET, nTaille);
	lire_image_pgm(nomImageDilateeEntree.c_str(), ImgInDilatee, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	difference(ImgInSeuillee, ImgInDilatee, ImgOut, nH, nW);
	
	ecrire_image_pgm(nomImageSortie.c_str(), ImgOut, nH, nW);
	
	free(ImgInSeuillee);
	free(ImgInDilatee);
	free(ImgOut);
	
	return 0;
}