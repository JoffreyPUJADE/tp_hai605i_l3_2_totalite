#include "Transformations.hpp"
#include <sstream>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 4)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageSeuilleeEntree nomImageDilateeEntree nomImageSortie";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageSeuilleeEntree = argv[1];
	std::string nomImageDilateeEntree = argv[2];
	std::string nomImageSortie = argv[3];
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_ppm(nomImageSeuilleeEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	int nTaille3 = nTaille * 3;
	OCTET *ImgInSeuillee, *ImgInDilatee, *ImgOut;
	
	allocation_tableau(ImgInSeuillee, OCTET, nTaille3);
	lire_image_ppm(nomImageSeuilleeEntree.c_str(), ImgInSeuillee, nH * nW);
	allocation_tableau(ImgInDilatee, OCTET, nTaille3);
	lire_image_ppm(nomImageDilateeEntree.c_str(), ImgInDilatee, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille3);
	
	differenceCouleur(ImgInSeuillee, ImgInDilatee, ImgOut, nH, nW);
	
	ecrire_image_ppm(nomImageSortie.c_str(), ImgOut, nH, nW);
	
	free(ImgInSeuillee);
	free(ImgInDilatee);
	free(ImgOut);
	
	return 0;
}