#include "Transformations.hpp"
#include <sstream>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 6)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageEntree nomImageSortie seuil1 seuil2 seuil3";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageEntree = argv[1];
	std::string nomImageSortie = argv[2];
	int s1 = type2Other<char*, int>(argv[3]); // Seuil 1.
	int s2 = type2Other<char*, int>(argv[4]); // Seuil 2.
	int s3 = type2Other<char*, int>(argv[5]); // Seuil 3.
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	OCTET *ImgIn, *ImgOut;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageEntree.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	seuil3(ImgIn, ImgOut, nH, nW, s1, s2, s3);
	
	ecrire_image_pgm(nomImageSortie.c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}