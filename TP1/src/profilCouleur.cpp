#include "Transformations.hpp"
#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 5)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageEntree nomProfilSortie c/l indice";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageEntree = argv[1];
	std::string nomProfilSortie = argv[2];
	char choixCL = argv[3][0];
	int indice = type2Other<char*, int>(argv[4]);
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_ppm(nomImageEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	int nTaille3 = nTaille * 3;
	OCTET *ImgIn;
	
	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(nomImageEntree.c_str(), ImgIn, nH * nW);
	
	std::vector<std::vector<lluint> > tabProfil = profilCouleur(ImgIn, nH, nW, choixCL, indice);
	
	std::ofstream ofs;
	
	ofs.open(nomProfilSortie);
	
	if(!ofs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : " << argv[0] << " : Impossible d'ouvrir le fichier \"" << nomProfilSortie << "\" en mode ecriture.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<tabProfil[0].size();++i)
		ofs << i << " " << tabProfil[0][i] << " " << tabProfil[1][i] << " " << tabProfil[2][i] << (i < (tabProfil[0].size() - 1) ? "\n" : "");
	
	ofs.close();
	
	free(ImgIn);
	
	return 0;
}