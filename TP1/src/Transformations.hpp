#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "image_ppm.h"
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

typedef long long unsigned int lluint;

// Fonctions définies pour convertir des types de variables vers d'autres.

template<typename T>
std::string type2String(T value)
{
	std::ostringstream oss;
	
	oss << value;
	
	return oss.str();
}

template<typename T>
T string2Type(std::string value)
{
	std::istringstream iss(value);
	
	T res;
	
	iss >> res;
	
	return res;
}

template<typename T, typename S>
S type2Other(T value)
{
	return string2Type<S>(type2String<T>(value));
}


// Fonctions définies pour le TP 1.

static inline void seuil1(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
			imgOut[i*nW+j] = imgIn[i*nW+j] < s ? 0 : 255;
	}
}

static inline void seuil2(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < s1)
				imgOut[i*nW+j] = 0;
			else
				imgOut[i*nW+j] = imgIn[i*nW+j] < s2 ? 128 : 255;
		}
	}
}

static inline void seuil3(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2, int s3)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < s1)
				imgOut[i*nW+j] = 0;
			else if(imgIn[i*nW+j] < s2)
				imgOut[i*nW+j] = 85;
			else
				imgOut[i*nW+j] = imgIn[i*nW+j] < s3 ? 170 : 255;
		}
	}
}

static inline void seuil1Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil1(rouge, rougeRes, nH, nW, s);
	seuil1(vert, vertRes, nH, nW, s);
	seuil1(bleu, bleuRes, nH, nW, s);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void seuil2Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil2(rouge, rougeRes, nH, nW, s1, s2);
	seuil2(vert, vertRes, nH, nW, s1, s2);
	seuil2(bleu, bleuRes, nH, nW, s1, s2);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline void seuil3Couleur(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int s1, int s2, int s3)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	OCTET *rougeRes(nullptr), *vertRes(nullptr), *bleuRes(nullptr);
	
	allocation_tableau(rougeRes, OCTET, nTaille);
	allocation_tableau(vertRes, OCTET, nTaille);
	allocation_tableau(bleuRes, OCTET, nTaille);
	
	seuil3(rouge, rougeRes, nH, nW, s1, s2, s3);
	seuil3(vert, vertRes, nH, nW, s1, s2, s3);
	seuil3(bleu, bleuRes, nH, nW, s1, s2, s3);
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	for(int i=0;i<nTaille;++i)
	{
		imgOut[3*i] = rougeRes[i];
		imgOut[3*i+1] = vertRes[i];
		imgOut[3*i+2] = bleuRes[i];
	}
	
	delete rougeRes;
	delete vertRes;
	delete bleuRes;
	
	rougeRes = nullptr;
	vertRes = nullptr;
	bleuRes = nullptr;
}

static inline std::vector<lluint> histo(OCTET* imgIn, int nH, int nW)
{
	lluint tabOcurrences[256] = {0};
	
	for(int i=0;i<(nH*nW);++i)
		++tabOcurrences[((lluint)imgIn[i])];
	
	std::vector<lluint> tabRes;
	
	for(int i=0;i<256;++i)
		tabRes.push_back(tabOcurrences[i]);
	
	return tabRes;
}

static inline std::vector<std::vector<lluint> > histoCouleur(OCTET* imgIn, int nH, int nW)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	std::vector<std::vector<lluint> > tabRes;
	
	tabRes.push_back(histo(rouge, nH, nW));
	tabRes.push_back(histo(vert, nH, nW));
	tabRes.push_back(histo(bleu, nH, nW));
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	return tabRes;
}

static inline std::vector<lluint> profil(OCTET* imgIn, int nH, int nW, char choixCL, int indice)
{
	if((choixCL != 'l') && (choixCL != 'c'))
		throw std::runtime_error("ERREUR : Profil : Le choix doit etre uniquement 'l' pour ligne ou 'c' pour colonne.");
	
	if((choixCL == 'l') && ((indice < 0) || (indice >= nH)))
		throw std::range_error("ERREUR : Profil : L'indice de la ligne choisi est en-dehors des bornes de l'image (qui est [0;hauteur de l'image[).");
	else if((choixCL == 'c') && ((indice < 0) || (indice >= nW)))
		throw std::range_error("ERREUR : Profil : L'indice de la colonne choisi est en-dehors des bornes de l'image (qui est [0;largeur de l'image[).");
	
	std::vector<lluint> tabRes;
	
	if(choixCL == 'l') // Si le type de profil choisi est la ligne, alors...
	{
		for(int j=0;j<nW;++j)
			tabRes.push_back(((lluint)imgIn[indice*nW+j]));
	}
	else // Sinon, si le type de profil choisi est la colonne, alors...
	{
		for(int i=0;i<nH;++i)
			tabRes.push_back(((lluint)imgIn[i*nW+indice]));
	}
	
	return tabRes;
}

static inline std::vector<std::vector<lluint> > profilCouleur(OCTET* imgIn, int nH, int nW, char choixCL, int indice)
{
	OCTET *rouge(nullptr), *vert(nullptr), *bleu(nullptr);
	int nTaille = nH * nW;
	
	allocation_tableau(rouge, OCTET, nTaille);
	allocation_tableau(vert, OCTET, nTaille);
	allocation_tableau(bleu, OCTET, nTaille);
	
	planR(rouge, imgIn, nTaille);
	planV(vert, imgIn, nTaille);
	planB(bleu, imgIn, nTaille);
	
	std::vector<std::vector<lluint> > tabRes;
	
	try
	{
		tabRes.push_back(profil(rouge, nH, nW, choixCL, indice));
		tabRes.push_back(profil(vert, nH, nW, choixCL, indice));
		tabRes.push_back(profil(bleu, nH, nW, choixCL, indice));
	}
	catch(std::runtime_error const& err)
	{
		delete rouge;
		delete vert;
		delete bleu;
		
		rouge = nullptr;
		vert = nullptr;
		bleu = nullptr;
		
		throw std::runtime_error(err.what());
	}
	
	delete rouge;
	delete vert;
	delete bleu;
	
	rouge = nullptr;
	vert = nullptr;
	bleu = nullptr;
	
	return tabRes;
}

#endif // TRANSFORMATIONS_HPP