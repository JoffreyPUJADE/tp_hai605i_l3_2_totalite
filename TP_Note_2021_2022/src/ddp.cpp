#include "Transformations.hpp"
#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageEntree nomDDPSortie";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageEntree = argv[1];
	std::string nomDDPSortie = argv[2];
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	OCTET *ImgIn;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageEntree.c_str(), ImgIn, nH * nW);
	
	std::vector<ldouble> tabDDP = ddp(ImgIn, nH, nW);
	
	std::ofstream ofs;
	
	ofs.open(nomDDPSortie);
	
	if(!ofs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : " << argv[0] << " : Impossible d'ouvrir le fichier \"" << nomDDPSortie << "\" en mode ecriture.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<tabDDP.size();++i)
		ofs << i << " " << tabDDP[i] << (i < (tabDDP.size() - 1) ? "\n" : "");
	
	ofs.close();
	
	free(ImgIn);
	
	return 0;
}