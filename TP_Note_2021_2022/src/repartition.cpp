#include "Transformations.hpp"
#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::ostringstream ossUtilisation;
		
		ossUtilisation << "Utilisation : " << argv[0] << " nomImageEntree nomRepartitionSortie";
		
		throw std::runtime_error(ossUtilisation.str().c_str());
	}
	
	std::string nomImageEntree = argv[1];
	std::string nomRepartitionSortie = argv[2];
	
	int nH, nW;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageEntree.c_str(), &nH, &nW);
	
	int nTaille = nH * nW;
	OCTET *ImgIn;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageEntree.c_str(), ImgIn, nH * nW);
	
	std::vector<ldouble> tabRepartition = repartition(ImgIn, nH, nW);
	
	std::ofstream ofs;
	
	ofs.open(nomRepartitionSortie);
	
	if(!ofs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : " << argv[0] << " : Impossible d'ouvrir le fichier \"" << nomRepartitionSortie << "\" en mode ecriture.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<tabRepartition.size();++i)
		ofs << i << " " << tabRepartition[i] << (i < (tabRepartition.size() - 1) ? "\n" : "");
	
	ofs.close();
	
	free(ImgIn);
	
	return 0;
}